package com.example.lukasz.zabytki.permission;

/**
 * Created by lukasz on 30.10.2017.
 */

public interface iPermissionManager {
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
}
