package com.example.lukasz.zabytki.monumentFragments;

import android.support.v4.app.Fragment;

/**
 * Created by lukasz on 06.11.2017.
 */

public interface iFragmentChange {
    public void showMapFragment();
}
