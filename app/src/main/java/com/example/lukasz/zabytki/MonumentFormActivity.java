package com.example.lukasz.zabytki;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lukasz.zabytki.enums.ButtonAction;
import com.example.lukasz.zabytki.models.Monument;

public class MonumentFormActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView name, street, city, description;
    private ButtonAction action;
    private DatabaseHandler db;
    private Monument monument;
    private int monument_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument_form);

        name = (TextView) findViewById(R.id.monumentName);
        street = (TextView) findViewById(R.id.monumentStreet);
        city = (TextView) findViewById(R.id.monumentCity);
        description = (TextView) findViewById(R.id.monumentDescription);
        db = new DatabaseHandler(this);

        Intent intent = getIntent();
        if(intent != null)
            monument_id = (int) intent.getExtras().getLong("MONUMENT_ID");

        if(monument_id == 0){
            action = ButtonAction.SAVE;
            monument = new Monument();
        }
        else {
            action = ButtonAction.UPDATE;
            monument = db.getMonument(monument_id);
            setUpTextViewsValues();
        }

        setButtonAction(action);
    }


    @Override
    public void onClick(View view) {
        String mName = name.getText().toString();
        String mStreet = street.getText().toString();
        String mCity = city.getText().toString();
        String mDescription = description.getText().toString();

        monument.setName(mName);
        monument.setStreet(mStreet);
        monument.setCity(mCity);
        monument.setDescription(mDescription);


        if(mStreet.isEmpty()){
            street.setError(getText(R.string.monument_street_required));
        }else if(mCity.isEmpty()){
            city.setError(getText(R.string.monument_city_required));
        }else{
            switch(action){
                case SAVE:
                    db.addMonument(monument);
                    finish();
                    break;
                case UPDATE:
                    db.updateMonument(monument);
                    finish();
                    break;
            }
        }
    }

    public void setButtonAction(ButtonAction action){
        this.action = action;
        Button actionButton = (Button) findViewById(R.id.monumentActionBTN);
        setTitle(action == ButtonAction.SAVE ? getText(R.string.new_monument) : getText(R.string.update_monument));
        actionButton.setText(action == ButtonAction.SAVE ? getString(R.string.save_monument) : getString(R.string.update_monument));
        actionButton.setOnClickListener(this);
    }

    public void setUpTextViewsValues(){
        monument = db.getMonument(monument_id);
        name.setText(monument.getName());
        street.setText(monument.getStreet());
        city.setText(monument.getCity());
        description.setText(monument.getDescription());
    }
}
