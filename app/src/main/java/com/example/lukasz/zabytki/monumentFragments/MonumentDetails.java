package com.example.lukasz.zabytki.monumentFragments;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lukasz.zabytki.R;

public class MonumentDetails extends Fragment implements View.OnClickListener{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monument_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView monumentName = (TextView) getView().findViewById(R.id.monumentNameET);
        TextView monumentDescription = (TextView) getView().findViewById(R.id.monumentDescription);
        TextView monumentLocation = (TextView) getView().findViewById(R.id.monumentLocation);
        ImageButton showOnMap = (ImageButton) getView().findViewById(R.id.showOnMap);


        monumentName.setText(getArguments().getString("Name"));
        monumentDescription.setText(getArguments().getString("Description"));
        monumentLocation.setText(getArguments().getString("Address"));

        showOnMap.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        iFragmentChange fc= (iFragmentChange)getActivity();
        fc.showMapFragment();
    }
}