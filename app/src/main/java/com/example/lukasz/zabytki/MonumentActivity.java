package com.example.lukasz.zabytki;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.lukasz.zabytki.models.Monument;
import com.example.lukasz.zabytki.monumentFragments.MonumentDetails;
import com.example.lukasz.zabytki.monumentFragments.MonumentMap;
import com.example.lukasz.zabytki.monumentFragments.iFragmentChange;
import com.example.lukasz.zabytki.permission.PermissionManager;


public class MonumentActivity extends AppCompatActivity implements iFragmentChange{

    private int monument_id = 0;
    private Bundle monument_data;
    private boolean canDisplayMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument);

        Bundle extras = getIntent().getExtras();

        if(extras != null)
            monument_id = extras.getInt("Monument_ID");

        Monument monument = new DatabaseHandler(this).getMonument(monument_id);
        setTitle(monument.getName());

        MonumentDetails monumentDetails = new MonumentDetails();

        monument_data = new Bundle();
        monument_data.putString("Name", monument.getName());
        monument_data.putString("Description", monument.getDescription());
        monument_data.putString("Address", monument.getAddress());

        monumentDetails.setArguments(monument_data);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_placeholder, monumentDetails, "details").commit();

        if(PermissionManager.hasPermissionTo(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            canDisplayMap = true;
            showMapFragment();
        }

    }


    @Override
    public void showMapFragment() {
        MonumentMap monumentMap = new MonumentMap();
        monument_data.putBoolean("canDisplayMap", canDisplayMap);
        monumentMap.setArguments(monument_data);
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (findViewById(R.id.fragment_map) != null ) {
            monumentMap.setArguments(monument_data);
            fragmentManager.beginTransaction().add(R.id.fragment_map, monumentMap, "map").commit();
        }
        if (findViewById(R.id.fragment_map) == null) {
            fragmentManager.beginTransaction().replace(R.id.fragment_placeholder, monumentMap, "map").addToBackStack("map").commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == 0) {
            canDisplayMap = true;
            showMapFragment();
        }else {
            Toast.makeText(this, getText(R.string.location_permission_disable) + " "
                    + getText(R.string.unable_to_show_map) + " "
                    +getText(R.string.enable_location_permission), Toast.LENGTH_LONG).show();

        }
    }

}
