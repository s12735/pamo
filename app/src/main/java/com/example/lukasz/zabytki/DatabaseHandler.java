package com.example.lukasz.zabytki;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.lukasz.zabytki.models.Monument;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukasz on 30.10.2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "myProject";

    // Monuments table name
    private static final String TABLE_MONUMENTS = "monuments";

    // Monuments Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_STREET = "street";
    private static final String KEY_POSTAL_CODE = "postal_code";
    private static final String KEY_CITY = "city";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MONUMENTS_TABLE = "CREATE TABLE " + TABLE_MONUMENTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_DESCRIPTION + " TEXT,"
                + KEY_STREET + " TEXT,"
                + KEY_POSTAL_CODE + " TEXT,"
                + KEY_CITY + " TEXT"
                + ")";
        db.execSQL(CREATE_MONUMENTS_TABLE);

        // ONLY FOR PRESENTATION ------------------------------------
        Monument monument = new Monument();
        monument.setCity("Gdańsk");
        monument.setStreet("Wały Jagiellońskie 2A");
        monument.setName("Brama wyżynna");
        monument.setDescription("Brama Wyżynna, dawniej także \"Brama Wysoka\" (niem. das Hohe Tor, kaszb. Wësokô Brąma]) – renesansowa brama miejska w Gdańsku, obecnie przy głównej trasie samochodowej (ulice Okopowa i Wały Jagiellońskie). Do 1895 znajdowała się w ciągu szesnastowiecznych fortyfikacji, pomiędzy Bastionem św. Elżbiety i Bastionem Karowym oraz stanowiła główną bramę wjazdową do miasta, otwierającą ciąg tzw. Drogi Królewskiej.Bramę zbudował do 1588 Willem van den Blocke. W dwudziestowiecznej literaturze rozpowszechnił się pogląd, że Bramę Wyżynną wybudował w latach 1574-1576 Hans Kramer, a następnie w latach 1587-1588 miała ona zostać oblicowana piaskowcem (rustykowana) przez W. van den Blocke'a. Franciszek Krzysiak wykazał, że jest to nieporozumienie – w rzeczywistości Kramer wybudował jedynie tzw. bramę wewnętrzną, która istniała do 1878, a następnie została wyburzona.");
        addMonument(monument);
        // ----------------------------------------------------------
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MONUMENTS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new monument
    void addMonument(Monument monument) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, monument.getName());
        values.put(KEY_DESCRIPTION, monument.getDescription());
        values.put(KEY_STREET, monument.getStreet());
        values.put(KEY_CITY, monument.getCity());
        values.put(KEY_POSTAL_CODE, monument.getPostal_code());

        // Inserting Row
        db.insert(TABLE_MONUMENTS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single monument
    public Monument getMonument(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MONUMENTS, new String[] { KEY_ID,
                        KEY_NAME, KEY_DESCRIPTION, KEY_STREET, KEY_CITY, KEY_POSTAL_CODE}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Monument monument = new Monument(cursor.getString(1), cursor.getString(2));
        monument.setID(cursor.getInt(0));
        monument.setStreet(cursor.getString(3));
        monument.setCity(cursor.getString(4));
        monument.setPostal_code(cursor.getString(5));
        return monument;
    }

    // Getting All Monuments
    public List<Monument> getAllMonuments() {
        List<Monument> monumentList = new ArrayList<Monument>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MONUMENTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Monument monument = new Monument();
                monument.setID(Integer.parseInt(cursor.getString(0)));
                monument.setName(cursor.getString(1));
                monument.setDescription(cursor.getString(2));
                monument.setStreet(cursor.getString(3));
                monument.setCity(cursor.getString(4));
                monument.setPostal_code(cursor.getString(5));
                monumentList.add(monument);
            } while (cursor.moveToNext());
        }

        // return monument list
        return monumentList;
    }

    // Updating single monument
    public int updateMonument(Monument monument) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, monument.getName());
        values.put(KEY_DESCRIPTION, monument.getDescription());
        values.put(KEY_STREET, monument.getStreet());
        values.put(KEY_CITY, monument.getCity());
        values.put(KEY_POSTAL_CODE, monument.getPostal_code());

        // updating row
        return db.update(TABLE_MONUMENTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(monument.getID()) });
    }

    // Deleting single monument
    public void deleteMonument(Monument monument) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MONUMENTS, KEY_ID + " = ?",
                new String[] { String.valueOf(monument.getID()) });
        db.close();
    }

    public void deleteMonument(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MONUMENTS, KEY_ID + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }


    // Getting monuments Count
    public int getMonumentsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MONUMENTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
}
