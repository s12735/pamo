package com.example.lukasz.zabytki.location;

/**
 * Created by lukasz on 13.11.2017.
 */

public interface iMyLocation {
    void calculateAndShowDistance();
    void showAlert();
}
