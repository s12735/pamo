package com.example.lukasz.zabytki.enums;

/**
 * Created by lukasz on 07.11.2017.
 */

public enum ButtonAction {
    UPDATE,
    SAVE
}
